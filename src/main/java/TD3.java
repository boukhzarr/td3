/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {

    if(a>b){
        return a;
    }
else return b;
}

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {
    int max = max2(a,b);
    if (c > max) {
        max = c;
    }
    return max;

}
/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
int nbChiffres (int n  ) {

}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */

int nbChiffresDuCarre(int n){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car){


}

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){


}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){


}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){


}

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */

void pyramide(int h ) {


}

/**
 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.
 */
int racineParfaite(int c ){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 *
 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 *
 * @param p
 * @param q
 * @return vrai  si deux nombres entiers `p`et `q` sont amis, faux sinon.
 */
boolean nombresAmicaux(int p , int q){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

/**
 * affiche à l'écran les couples de
 *nombres amis parmi les nombres inférieurs ou égaux à max.
 * @param max
 */

void afficheAmicaux(int max){

}

/**
 *
 * @param c1
 * @param c2
 * @return vrai si deux entiers donnés peuvent  être les côtés de l'angle droit d'un triangle rectangle dont les 3
 *        côtés sont des nombres entiers, faux sinon
 */
boolean  estTriangleRectangle (int c1, int c2){

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");

}

void main (){


}
